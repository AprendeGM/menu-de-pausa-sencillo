{
    "id": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ball_guy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5591efa3-78cd-411d-a2e1-e74eaa7311a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "cd508861-9b73-4a8e-a349-d5eed528654b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5591efa3-78cd-411d-a2e1-e74eaa7311a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c9d68ae-9e75-43ba-9015-cea4fabcdba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5591efa3-78cd-411d-a2e1-e74eaa7311a8",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "ca8935f5-8072-4055-a316-7dfd6bec030b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "f2f3badc-f668-4c40-91f4-a75de99120fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca8935f5-8072-4055-a316-7dfd6bec030b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00b8f137-9217-4d28-883c-caec61e6d8df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca8935f5-8072-4055-a316-7dfd6bec030b",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "e3219453-4b0b-4d28-9f25-ad1c3d2b2dd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "8b97a0dc-c261-49a2-9ebd-6fab29411bc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3219453-4b0b-4d28-9f25-ad1c3d2b2dd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f14d781-cae4-4023-8c67-69c8aaf7c525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3219453-4b0b-4d28-9f25-ad1c3d2b2dd7",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "6dc0a5a7-1587-4050-9635-6f1ba7f713ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "1ff5b41f-4375-40fe-b13f-f5b72d7917b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dc0a5a7-1587-4050-9635-6f1ba7f713ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6323b252-7ce1-4ecf-a4ca-a775ef6a41a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dc0a5a7-1587-4050-9635-6f1ba7f713ca",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "5aef8bfa-5bad-4b6a-ace8-2e2d078f3a6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "b8082917-0415-46d6-ad6d-16ee42a461a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aef8bfa-5bad-4b6a-ace8-2e2d078f3a6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1b08cf7-f5da-40a7-ab70-ae9a771dac18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aef8bfa-5bad-4b6a-ace8-2e2d078f3a6e",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "21db3e10-7257-4b2c-b644-a68450391cf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "05191b8e-dd10-4d5c-a3c2-c412124b94ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21db3e10-7257-4b2c-b644-a68450391cf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce8237a0-b87e-4d92-a8d8-4f992d5a430e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21db3e10-7257-4b2c-b644-a68450391cf3",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "fd3a3fa4-900a-4e9d-8caa-1c7d9944656f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "52fb63e3-6284-4aa5-9089-628ae671c252",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd3a3fa4-900a-4e9d-8caa-1c7d9944656f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69b639c8-d044-40ea-9e3a-b155b7a072ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd3a3fa4-900a-4e9d-8caa-1c7d9944656f",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "71c18c76-27f9-49fa-8a9b-ee745b97d2a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "20ae6735-f588-4bf2-88fa-35e573503d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71c18c76-27f9-49fa-8a9b-ee745b97d2a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5af10c-b836-44e2-9369-1f7c0c208984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71c18c76-27f9-49fa-8a9b-ee745b97d2a5",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "44f7eafb-2042-41f6-a73d-25a79f02dc05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "69601986-0543-4fbd-b95b-ab2938f6172b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44f7eafb-2042-41f6-a73d-25a79f02dc05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4903bcb9-480a-4523-8b82-1391921bd0f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44f7eafb-2042-41f6-a73d-25a79f02dc05",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "4bbcd1d1-737f-4375-b857-6febd7bfb897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "2c901d30-91a9-4890-bac6-bb1d55827ee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bbcd1d1-737f-4375-b857-6febd7bfb897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed823538-b904-41e1-ae59-4e3c310bb352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bbcd1d1-737f-4375-b857-6febd7bfb897",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "68d2e6f3-ff5b-4e17-8d87-5490f0f3bcd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "7fb86acd-ca8a-4048-b067-1f780bd1a9fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d2e6f3-ff5b-4e17-8d87-5490f0f3bcd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5d2787-c968-4a20-8662-ac6afb516063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d2e6f3-ff5b-4e17-8d87-5490f0f3bcd6",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "a51eeb4e-350e-460c-a496-d1ffd2264d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "3f6c2dee-26b0-4bb1-a356-7aa1c87dbbce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a51eeb4e-350e-460c-a496-d1ffd2264d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b554e8d-6fcc-4539-b3bf-78710d1a8b00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a51eeb4e-350e-460c-a496-d1ffd2264d2e",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "c0aab4d4-b0d3-4302-94af-712f1e1c0331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "d8c1c9f6-ec87-4dad-9842-2dd8b33a2861",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0aab4d4-b0d3-4302-94af-712f1e1c0331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77577b46-ad3e-460e-99ca-c9bf3293e47f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0aab4d4-b0d3-4302-94af-712f1e1c0331",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "b804d8ac-fdc0-4316-8d14-b13f212de7c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "56513c36-3790-4095-bb5d-253db47b9b77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b804d8ac-fdc0-4316-8d14-b13f212de7c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b02b25e8-29b7-4621-bcf1-9df94770ea7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b804d8ac-fdc0-4316-8d14-b13f212de7c5",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "045b1e67-5c0b-4e4f-a01b-98b2e2321ae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "34dd5bb0-2239-495d-a096-964bec88cce6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "045b1e67-5c0b-4e4f-a01b-98b2e2321ae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd031dd-70d5-4b74-b31f-0617fa8b367f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "045b1e67-5c0b-4e4f-a01b-98b2e2321ae0",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "309d0e2a-7c14-4dd1-95af-75a56da0b687",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "3fd079dc-c618-4a0d-ad6f-4f36236054ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "309d0e2a-7c14-4dd1-95af-75a56da0b687",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f305d107-b3be-4cde-ade1-af6caf3c1a02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "309d0e2a-7c14-4dd1-95af-75a56da0b687",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "48dfc236-e5d2-43bf-9aa9-337bb31be33d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "bd99af9a-26df-4cd2-94f5-6836176fae57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48dfc236-e5d2-43bf-9aa9-337bb31be33d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61db1b18-2c8c-4393-a7dc-54313ab9ce74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48dfc236-e5d2-43bf-9aa9-337bb31be33d",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "48bb716f-67f7-4425-b61f-bdbbf154e420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "4feea8bb-05e8-4bf8-bceb-dc801ad97db0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48bb716f-67f7-4425-b61f-bdbbf154e420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a359e23-a994-49b7-8942-1e683ea6e39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48bb716f-67f7-4425-b61f-bdbbf154e420",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "f245b0b8-1b49-4bcd-bee7-377999b2a9cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "f26ee280-b082-4f79-95b7-958436c48c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f245b0b8-1b49-4bcd-bee7-377999b2a9cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b08dc105-e3cc-404d-a8dc-50d521942cb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f245b0b8-1b49-4bcd-bee7-377999b2a9cd",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "740275a3-dddf-49de-9baa-bd427f40d99c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "b468b500-e6e6-48b9-9bb0-f3a694983e7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740275a3-dddf-49de-9baa-bd427f40d99c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e7a1c96-7cd1-4638-95ac-70e7cc833403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740275a3-dddf-49de-9baa-bd427f40d99c",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "8396ad2c-9203-4bbd-abf3-e57a1b47be1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "8efbdd17-8e83-45f7-9551-d0a7dfaf9348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8396ad2c-9203-4bbd-abf3-e57a1b47be1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dd2a71c-f2c3-4baf-80c8-6292e76b6687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8396ad2c-9203-4bbd-abf3-e57a1b47be1c",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "9b947050-b708-4c67-97e4-b2f7eb21e515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "bf07fbd6-4194-4787-888e-6ec9778c5dc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b947050-b708-4c67-97e4-b2f7eb21e515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6de275db-177a-40d0-b56b-d280cbfa15d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b947050-b708-4c67-97e4-b2f7eb21e515",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "606d0c4c-d298-47a7-a42d-358ab7a0582d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "ae68d481-9213-4088-99df-e1f2aa8a9d3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "606d0c4c-d298-47a7-a42d-358ab7a0582d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "971a08f9-b9a7-4e37-b8f5-fb0f0c21de08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "606d0c4c-d298-47a7-a42d-358ab7a0582d",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "bfcbad7d-f844-4259-8dc0-3c32589d6e03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "88198963-3025-4867-be6b-8147ed712849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfcbad7d-f844-4259-8dc0-3c32589d6e03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cadbb71-c7bb-47b5-b2da-91d27a7e3679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfcbad7d-f844-4259-8dc0-3c32589d6e03",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "bafb9064-b460-482d-8771-6a67043ea7f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "6f27149e-e6e3-4de4-99ee-e61a9feb866f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bafb9064-b460-482d-8771-6a67043ea7f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e476ce30-0481-4bec-989c-b9ea1a361f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bafb9064-b460-482d-8771-6a67043ea7f6",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "2d378b4f-a3ee-4a96-ac11-2941e6bf7d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "cb474a29-ef43-44d5-9fd9-efa6efa46506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d378b4f-a3ee-4a96-ac11-2941e6bf7d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28b60eb0-20f8-4707-8ebf-efee5dfd6d55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d378b4f-a3ee-4a96-ac11-2941e6bf7d4d",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "02842cc8-a664-43bb-91f3-55a849ea3717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "c7b5fc11-4d09-4146-b98b-080a79c1dd25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02842cc8-a664-43bb-91f3-55a849ea3717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f434ca49-db22-4871-97e2-7bd2b8ccc6b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02842cc8-a664-43bb-91f3-55a849ea3717",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        },
        {
            "id": "32a1abfb-2f06-43be-b6bd-34b743da99f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "compositeImage": {
                "id": "c615edcf-fcc7-4827-b751-c0c116c3e8a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32a1abfb-2f06-43be-b6bd-34b743da99f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3a2dbcd-b50b-4908-9011-f4b90108f7cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32a1abfb-2f06-43be-b6bd-34b743da99f8",
                    "LayerId": "cbf75f9c-6685-4489-b719-672a38f87651"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "cbf75f9c-6685-4489-b719-672a38f87651",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
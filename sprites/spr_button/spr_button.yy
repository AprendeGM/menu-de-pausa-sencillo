{
    "id": "34352895-768c-4a7b-84f2-99bbd7fe9cf9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 189,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "904b1348-febd-4900-bdf3-fdc844666b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34352895-768c-4a7b-84f2-99bbd7fe9cf9",
            "compositeImage": {
                "id": "8c3a91b8-9392-48d9-81d8-27662fd4a921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "904b1348-febd-4900-bdf3-fdc844666b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f7fabd3-8b8a-44b6-a7d5-782588637802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "904b1348-febd-4900-bdf3-fdc844666b57",
                    "LayerId": "574cb18b-a5a1-4192-a8f4-01548b57a151"
                }
            ]
        },
        {
            "id": "643786d9-50e7-4f96-a416-dee7fedee8d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34352895-768c-4a7b-84f2-99bbd7fe9cf9",
            "compositeImage": {
                "id": "a197b8f5-2f00-4321-ab54-e3cb6a03f29e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "643786d9-50e7-4f96-a416-dee7fedee8d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6322ffec-b7f6-4621-8ace-f03db075d917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "643786d9-50e7-4f96-a416-dee7fedee8d6",
                    "LayerId": "574cb18b-a5a1-4192-a8f4-01548b57a151"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "574cb18b-a5a1-4192-a8f4-01548b57a151",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34352895-768c-4a7b-84f2-99bbd7fe9cf9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 190,
    "xorig": 95,
    "yorig": 24
}
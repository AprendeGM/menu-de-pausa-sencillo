{
    "id": "8b7a657d-29f4-404a-a2ac-e3072fea3f1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pause",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 11,
    "bbox_right": 38,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8db83ba5-3fe4-4368-b096-3d9f4c18cee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b7a657d-29f4-404a-a2ac-e3072fea3f1e",
            "compositeImage": {
                "id": "82c2fd36-1364-41fc-bb68-093a3f72c377",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8db83ba5-3fe4-4368-b096-3d9f4c18cee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b76870d-b72e-46b1-9083-dc57297f0edc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8db83ba5-3fe4-4368-b096-3d9f4c18cee8",
                    "LayerId": "f84b00d9-571c-4b14-8232-0c074a8a3bfc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "f84b00d9-571c-4b14-8232-0c074a8a3bfc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b7a657d-29f4-404a-a2ac-e3072fea3f1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}
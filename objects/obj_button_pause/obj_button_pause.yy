{
    "id": "ae1e5535-1417-470c-b93b-8ce14b678cde",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_pause",
    "eventList": [
        {
            "id": "02316e53-3d1c-4a80-a4d9-0e93f89ea2cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "ae1e5535-1417-470c-b93b-8ce14b678cde"
        },
        {
            "id": "8d44c227-89f7-4274-8d26-4ed2bf850671",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "ae1e5535-1417-470c-b93b-8ce14b678cde"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "a2dbbd23-7e11-42af-9743-410bfd343ddd",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "room",
            "varName": "global.room_actual",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "8b7a657d-29f4-404a-a2ac-e3072fea3f1e",
    "visible": true
}
{
    "id": "41172b50-f814-4231-926f-1a97146babef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_return",
    "eventList": [
        {
            "id": "2065974b-706f-4c63-82d3-5bc33c699144",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "41172b50-f814-4231-926f-1a97146babef"
        },
        {
            "id": "acf9252d-16b5-4b4a-8506-c9798f95c61b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "41172b50-f814-4231-926f-1a97146babef"
        },
        {
            "id": "105db6b6-297f-4973-ac56-9b114d5d0fe7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "41172b50-f814-4231-926f-1a97146babef"
        },
        {
            "id": "96b35bd4-2483-4d07-8c12-780c07b1105a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "41172b50-f814-4231-926f-1a97146babef"
        },
        {
            "id": "568b7d6b-542d-4842-958e-0eee1da0d82a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "41172b50-f814-4231-926f-1a97146babef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "34352895-768c-4a7b-84f2-99bbd7fe9cf9",
    "visible": true
}
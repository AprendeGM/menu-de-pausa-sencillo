/// @DnDAction : YoYo Games.Movement.Reverse
/// @DnDVersion : 1
/// @DnDHash : 0A07795F
direction = (direction + 180) % 360;

/// @DnDAction : YoYo Games.Instances.Sprite_Scale
/// @DnDVersion : 1
/// @DnDHash : 28D3FF9C
/// @DnDArgument : "xscale" "-1"
image_xscale = -1;
image_yscale = 1;
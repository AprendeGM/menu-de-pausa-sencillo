/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4A64628A
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "room_game1"
if(room == room_game1)
{
	/// @DnDAction : YoYo Games.Rooms.Go_To_Room
	/// @DnDVersion : 1
	/// @DnDHash : 6846DEE1
	/// @DnDParent : 4A64628A
	/// @DnDArgument : "room" "room_game2"
	/// @DnDSaveInfo : "room" "a48879bc-d106-4595-975e-47fcb03e930e"
	room_goto(room_game2);
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 52282C60
else
{
	/// @DnDAction : YoYo Games.Rooms.Go_To_Room
	/// @DnDVersion : 1
	/// @DnDHash : 03ED2687
	/// @DnDParent : 52282C60
	/// @DnDArgument : "room" "room_game1"
	/// @DnDSaveInfo : "room" "1cb672d8-40f6-4c28-8a2a-67d1a51e0697"
	room_goto(room_game1);
}
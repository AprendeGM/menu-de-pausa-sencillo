{
    "id": "2fefdfc7-904f-4c56-8c48-8a594947890f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ball_guy",
    "eventList": [
        {
            "id": "8ff6e96e-b5d4-499d-83cd-f990c960e992",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2fefdfc7-904f-4c56-8c48-8a594947890f"
        },
        {
            "id": "664461bf-54be-4e52-b725-84f0b3e578c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "2fefdfc7-904f-4c56-8c48-8a594947890f"
        },
        {
            "id": "7e22a061-2ae0-47cb-8e4f-d67ca70d3c0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "2fefdfc7-904f-4c56-8c48-8a594947890f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b0de323-378e-460f-892a-9f7ddf1d2ccf",
    "visible": true
}